const originalFriendsData = ['Аня', 'Оля', 'Влада', 'Славко', 'Андрій', 'Павло', 'Тетяна', 'Володимир'];

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.static('public'));
app.use(bodyParser.json());

let friendsData = ['Аня', 'Оля', 'Влада', 'Славко', 'Андрій', 'Павло', 'Тетяна', 'Володимир'];

app.post('/saveData', (req, res) => {
  friendsData = req.body.friends;
  res.sendStatus(200);
});

app.get('/getData', (req, res) => {
  res.json({ friends: friendsData });
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});


app.get('/getOriginalData', (req, res) => {
  res.json({ friends: originalFriendsData });
});